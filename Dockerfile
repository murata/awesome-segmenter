# Use the official Python image as the base image
FROM python:3.9-slim
# Install system dependencies
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends libx11-dev \
                                                python3-tk \
                                                git \
                                                libegl1 \
                                                libgl1 \
                                                libgomp1 \
                                                libhdf5-dev
RUN rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip

COPY requirements.txt /tmp/requirements.txt
# Upgrade pip and install Python dependencies
RUN pip install --no-cache-dir -r /tmp/requirements.txt

COPY requirements.txt /tmp/torch_requirements.txt
# Upgrade pip and install Python dependencies
RUN pip install --no-cache-dir -r /tmp/torch_requirements.txt

# Set the working directory in the container
WORKDIR /app
# Run the Python script
# CMD ["python", "plot.py"]
CMD ["/bin/bash"]
