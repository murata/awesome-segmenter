# Build Status
[![pipeline status](https://code.vt.edu/murata/awesome-segmenter/badges/main/pipeline.svg)](https://code.vt.edu/murata/awesome-segmenter/-/commits/main)
[![coverage report](https://code.vt.edu/murata/awesome-segmenter/badges/main/coverage.svg)](https://code.vt.edu/murata/awesome-segmenter/-/commits/main)
[![Latest Release](https://code.vt.edu/murata/awesome-segmenter/-/badges/release.svg)](https://code.vt.edu/murata/awesome-segmenter/-/releases)


# awesome-segmenter
This repository contains a semantic segmentation algorithm called `awesome-segmenter`.
This segmentation algorithm was developed by [Murat Ambarkutuk](https://murat.ambarkutuk.com) for Cyngn as part of their hiring process to the Sr. Perception Engineer position.

Below is the table of contents of this documentation file.

[[_TOC_]]

## Summary
`awesome-segmenter` is an unsupervised point cloud segmentation algorithm that is based-on clustering idea.


# Getting Started
## Local Use:
### Requirements and Dependencies (Deb-based Linux Instructions)
####
Run the following commands with ```sudo``` priviligies.
```bash
apt update -y
```
followed by
```bash
apt install -y libx11-dev python3-tk git libegl1 libgl1 libgomp1 libhdf5-dev
```

Finally, install the necessary `python` packages. The code uses `dataclass` objects to describe different objects. Therefore, we have a compatibility with Python version >= 3.8.

```python
pip install -r requirements.txt
```
## Dockerized Use:
```
docker-compose build
```
or, alternatively,

```Makefile
make build
```
## License


# Instructions Received from Cyngn
## Project Details
The details of the take-home challenge is given below.

## Problem Description

You have a LiDAR scanning the environment and you received a pointcloud with each point having attributes (x, y, z) in sensor coordinates.
The goal is to segment the pointcloud so as to identify the objects in the scene.

- Each segment should be assigned a unique ID.
- All points belonging to a segment should share the same ID as the segment.
- Each point should be associated with only one segment.

## Things to Consider

- Consider only the points that don't belong to the ground plane for the segmentation task.

## Implementation Details

- Folder `data` contains a pointcloud data.
- File `implementation.py` has been setup to load pointcloud from folder `data` into a numpy array and visualization has been provided to visualize how input 3D pointcloud looks like.

## Expectation

- It is expected that the core-parts of the algorithm to be written from scratch.
- 3rd-party libraries can be used for parts of the algorithm, like Linear Algebra routines, optimization routines, etc.
- Provide explanation as to why a specific algorithm has been selected over others methods.

## Example

Input:

```python
pointcloud = [
              [0.0, 0.0, 0.0],    # Point-1
              [2.0, 2.0, 2.0],    # Point-2
              [2.16, 4.89, 5.78], # Point-3
              [0.01, 0.1, 0.03],  # Point-4
              # ...
              ]
```

Output:

```python
segment_ids = [0,     # id = 0 as Point-1 belongs to segment-0
               1,     # id = 1 as Point-2 belongs to segment-1
               2,     # id = 2 as Point-3 belongs to segment-2
               0,     # id = 0 as Point-4 belongs to segment-0
              #...
              ]
```

`len(segment_ids)` is same as `len(pointcloud)`
