from typing import Any, List, Optional, Tuple

import numpy as np
import open3d as o3d
from sklearn.base import BaseEstimator
from sklearn.cluster import DBSCAN
from tqdm import tqdm

# from .pointnet2_sem_seg import get_model


class BaseSegmenter(BaseEstimator):
    """A base class for point cloud segmentation models.

    This class is intended to be inherited by specific segmentation
    strategies, providing a common interface for segmentation tasks.
    """

    def __init__(self):
        super().__init__()

    def transform(self, X: np.ndarray) -> np.ndarray:
        """Identity transformation. Returns the input array unchanged.

        Parameters:
            X (np.ndarray): The input data array.

        Returns:
            np.ndarray: The unchanged input data array.
        """
        return X

    def fit(self, X: np.ndarray, y: Optional[np.ndarray] = None) -> None:
        """Placeholder for fit method. Raises NotImplementedError.

        Parameters:
            X (np.ndarray): The input data array.
            y (Optional[np.ndarray]): The target values (not used).

        Raises:
            NotImplementedError: Always raised to indicate that this method
                                 should be implemented by the derived class.
        """
        raise NotImplementedError("In case we want to try some supervised method")

    def predict(self, X: np.ndarray, **kwargs: Any) -> np.ndarray:
        """Placeholder for predict method. Raises NotImplementedError.

        Parameters:
            X (np.ndarray): The input data array.
            **kwargs (Any): Additional arguments for the prediction.

        Raises:
            NotImplementedError: Always raised to indicate that this method
                                 should be implemented by the derived class.
        """
        raise NotImplementedError("Should be implemented by the derived class")

    def validate_input(self, pointcloud: np.ndarray) -> np.ndarray:
        """Validates the input point cloud data.

        Ensures the input is a NumPy array of shape (n, 3).

        Parameters:
            pointcloud (np.ndarray): The input point cloud data.

        Returns:
            np.ndarray: The validated point cloud data.

        Raises:
            AssertionError: If the input does not meet the shape requirements.
        """
        pointcloud = np.asarray(pointcloud)
        assert (
            pointcloud.ndim == 2 and pointcloud.shape[1] == 3
        ), "Point cloud must be of shape (n, 3)"
        return pointcloud


class DBScanSegmenter(BaseSegmenter):
    """Implements DBSCAN clustering for point cloud segmentation.

    Attributes:
        eps (float): The maximum distance between two samples for one to be considered as in the neighborhood of the other.
        min_points (int): The number of samples in a neighborhood for a point to be considered as a core point.
    """

    def __init__(self, eps=0.5, min_points=5):
        """Initializes the DBScanSegmenter with specified parameters.

        Parameters:
            eps (float): The maximum distance between two samples.
            min_points (int): The minimum number of points to form a dense region.
        """
        super().__init__()
        self.eps = eps
        self.min_points = min_points

    def predict(self, pointcloud, **kwargs):
        """Segments the point cloud using DBSCAN clustering.

        Parameters:
            pointcloud (np.ndarray): The input point cloud data.
            **kwargs (Any): Optional keyword arguments to override eps and min_points.

        Returns:
            np.ndarray: An array of cluster labels for each point in the pointcloud.
        """
        eps = kwargs.get("eps", self.eps)
        min_points = kwargs.get("min_points", self.min_points)
        labels = DBSCAN(eps=eps, min_samples=min_points).fit_predict(pointcloud)
        return labels


class BaseClutterSegmenter(BaseSegmenter):
    """A base class for segmenting point clouds based on clutter characteristics.

    This class provides a method to segment point clouds by identifying regions
    that are likely to be clutter based on statistical and geometrical properties.
    """

    def __init__(self):
        super().__init__()

    def predict(
        self,
        pointcloud: np.ndarray,
        neighbours: int = 5,
        sigma_ratio: float = 2,
        radius: float = 3,
    ) -> np.ndarray:
        pointcloud = self.validate_input(pointcloud)
        labels = np.zeros(pointcloud.shape[0], dtype=np.int16)
        mask_statistical = np.zeros(pointcloud.shape[0], dtype=np.int16)
        mask_spherical = np.zeros(pointcloud.shape[0], dtype=np.int16)
        mask_low_incidence = np.zeros(pointcloud.shape[0], dtype=np.int16)

        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(pointcloud)
        _, statistical_inliers = pcd.remove_statistical_outlier(
            nb_neighbors=neighbours, std_ratio=sigma_ratio
        )
        mask_statistical[statistical_inliers] = 1
        mask_statistical = np.logical_not(mask_statistical)
        _, spherical_inliers = pcd.remove_radius_outlier(
            nb_points=neighbours, radius=radius
        )
        mask_spherical[spherical_inliers] = 1
        mask_spherical = np.logical_not(mask_spherical)

        # Estimate normals
        pcd.estimate_normals(
            search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30)
        )

        # Assume sensor position is at the origin (0, 0, 0)
        sensor_position = np.array([0, 0, 0])

        # Calculate incidence angles
        incidence_angles = []
        for point, normal in zip(pcd.points, pcd.normals):
            line_of_sight = point - sensor_position
            line_of_sight_normalized = line_of_sight / np.linalg.norm(line_of_sight)
            normal_normalized = normal / np.linalg.norm(normal)
            angle = np.arccos(np.dot(line_of_sight_normalized, normal_normalized))
            incidence_angles.append(angle)

        incidence_angles = np.array(incidence_angles) * 180.0 / np.pi
        crit_angles = np.quantile(incidence_angles, [0.05, 0.995])
        mask_low_incidence[incidence_angles < crit_angles[0]] = True
        mask_low_incidence[incidence_angles > crit_angles[1]] = True
        labels[mask_statistical] = 1
        labels[mask_spherical] = 2
        labels[mask_low_incidence] = 3
        return labels


class PlaneSegmenter(BaseSegmenter):
    """A class for segmenting point clouds based on planar regions.

    Attributes:
        tol (float): Tolerance for determining whether a point belongs to a plane.
        alpha (float): Weighting factor for balancing distance and angle criteria.
    """

    def __init__(self, plane_args, tol: float = 5e-1, alpha: float = 0.8):
        """Initializes the PlaneSegmenter with specified parameters.

        Parameters:
            tol (float): Tolerance for plane segmentation.
            alpha (float): Weighting factor for distance and angle criteria.
        """
        super().__init__()
        self.tol = tol
        self.alpha = alpha
        self.plane_args = plane_args

    def predict(self, pointcloud: np.ndarray, **kwargs) -> np.ndarray:
        """Segments the point cloud based on planar regions.

        Parameters:
            pointcloud (np.ndarray): The input point cloud data.
            plane_args (Tuple[float, float, float, float]): The coefficients (a, b, c, d) of the plane equation ax + by + cz + d = 0.

        Returns:
            np.ndarray: An array of labels indicating whether each point belongs to the plane (1) or not (0).
        """
        pointcloud = self.validate_input(pointcloud)
        labels = np.zeros(pointcloud.shape[0], dtype=np.int16)
        # Estimate surface normals
        normals = self.estimate_normals(pointcloud)
        plane_args = kwargs.get("plane_args", self.plane_args)
        # Calculate orthogonal distances
        d_ortho_vec = [
            self.plane_to_point_distance(plane_args, point) for point in pointcloud
        ]
        d_ortho_vec = np.array(d_ortho_vec)

        # Calculate angle between normals
        normal_angle_vec = [
            self.angle_between_vectors(normals[i], plane_args[:3])
            for i in range(len(normals))
        ]
        normal_angle_vec = np.array(normal_angle_vec)

        # Define the cost vector
        J = self.alpha * np.abs(d_ortho_vec - 0.2) + (
            1 - self.alpha
        ) * normal_angle_vec / np.max(normal_angle_vec)

        # Assign labels based on the tolerance
        labels = (J <= self.tol).astype(int)
        return labels

    @staticmethod
    def plane_to_point_distance(
        plane_args: Tuple[float, float, float, float], point: np.ndarray
    ) -> float:
        """Calculates the orthogonal distance from a point to a plane.

        Parameters:
            plane_args (Tuple[float, float, float, float]): The coefficients (a, b, c, d) of the plane equation.
            point (np.ndarray): The coordinates (x, y, z) of the point.

        Returns:
            float: The orthogonal distance from the point to the plane.
        """
        a, b, c, d = plane_args
        x, y, z = point
        return abs(a * x + b * y + c * z + d) / np.sqrt(a**2 + b**2 + c**2)

    @staticmethod
    def angle_between_vectors(v1: np.ndarray, v2: np.ndarray) -> float:
        """Calculates the angle between two vectors.

        Parameters:
            v1 (np.ndarray): The first vector.
            v2 (np.ndarray): The second vector.

        Returns:
            float: The angle between the vectors in radians.
        """
        # Consider the absolute value of the cosine of the angle
        cos_angle = abs(np.dot(v1, v2) / (np.linalg.norm(v1) * np.linalg.norm(v2)))
        return np.arccos(np.clip(cos_angle, -1, 1))  # Clip to handle numerical errors

    @staticmethod
    def estimate_normals(pointcloud: np.ndarray) -> np.ndarray:
        """Estimates the normals of a point cloud.

        Parameters:
            pointcloud (np.ndarray): The input point cloud data.

        Returns:
            np.ndarray: The estimated normals of the point cloud.
        """
        # Convert the numpy array to an Open3D point cloud
        pcd = o3d.geometry.PointCloud()
        pcd.points = o3d.utility.Vector3dVector(pointcloud)

        # Estimate normals
        pcd.estimate_normals(
            search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30)
        )

        # Extract normals from the point cloud
        normals = np.asarray(pcd.normals)
        return normals


# import torch
# class Pointnet2Segmenter(BaseSegmenter):
#     def __init__(self, model_params_path):
#         super().__init__()
#         self.model_params_path = model_params_path
#         self.model = get_model(13)
#         __state = torch.load("data/best_model.pth", map_location=torch.device("cpu"))

#         self.model.load_state_dict(__state["model_state_dict"])
#         self.model.eval()

#     def predict(self, pointcloud: np.ndarray) -> np.ndarray:
#         """Segments the point cloud using the PointNet++ model.

#         Parameters:
#             pointcloud (np.ndarray): The input point cloud data.

#         Returns:
#             np.ndarray: An array of predicted labels for each point in the point cloud.
#         """
#         pointcloud = self.validate_input(pointcloud)
#         pointcloud_tensor = (
#             torch.tensor(pointcloud).unsqueeze(0).transpose(1, 2).float()
#         )
#         labels, _ = self.model(pointcloud_tensor)
#         predicted_labels = torch.argmax(labels, dim=2)
#         return predicted_labels.squeeze().numpy()


class SegmentationPipeline:
    def __init__(self, segmenters: List[Tuple[str, object]]):
        """Initializes the segmentation pipeline with predefined segmenters.

        Args:
            segmenters (List[Tuple[str, object]]): A list of tuples, where each tuple contains a name and a segmenter object.
        """
        self._segmenters = segmenters

    @property
    def segmenters(self) -> List[Tuple[str, object]]:
        """Returns the list of segmenters in the pipeline."""
        return self._segmenters

    def __call__(self, pointcloud: np.ndarray) -> np.ndarray:
        """Applies the segmentation pipeline to the input point cloud.

        Args:
            pointcloud (np.ndarray): The input point cloud of shape (N, 3).

        Returns:
            np.ndarray: An array of labels indicating the segment for each point in the point cloud.
        """
        labels = np.zeros(pointcloud.shape[0], dtype=int)
        remaining_indices = np.arange(pointcloud.shape[0])

        for name, segmenter in tqdm(self._segmenters, desc="Segmenting"):
            print(f"Applying {name} segmenter...")
            mask = segmenter.predict(pointcloud[remaining_indices])
            labels[remaining_indices[mask != 0]] = len(np.unique(labels)) + 1
            remaining_indices = remaining_indices[mask == 0]

        return labels
