import unittest

import numpy as np

from segmentation import *


class TestSegmenters(unittest.TestCase):
    def setUp(self):
        # Set up test point cloud
        self.pointcloud = np.load("data/cyngn_interview_question_pointcloud_data.npy")

    def test_pipeline(self):
        pipeline = SegmentationPipeline(
            [
                ("clutter", BaseClutterSegmenter()),
                ("wall", PlaneSegmenter([0, 1, 0, -15])),
                ("floor", PlaneSegmenter([0, 0, 1, 1.5])),
                ("dbscan", DBScanSegmenter(1.75, 48)),
            ]
        )
        labels = pipeline(self.pointcloud)
        self.assertEqual(len(labels), len(self.pointcloud))
        self.assertTrue(np.all(labels >= 0))


if __name__ == "__main__":
    unittest.main()
