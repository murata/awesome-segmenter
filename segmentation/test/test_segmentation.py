import unittest

import numpy as np

from segmentation import *


class TestSegmenters(unittest.TestCase):
    def setUp(self):
        # Set up test point cloud
        self.pointcloud = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

    def test_base_segmenter(self):
        segmenter = BaseSegmenter()
        with self.assertRaises(NotImplementedError):
            segmenter.fit(self.pointcloud)
        with self.assertRaises(NotImplementedError):
            segmenter.predict(self.pointcloud)
        transformed = segmenter.transform(self.pointcloud)
        np.testing.assert_array_equal(transformed, self.pointcloud)

    def test_dbscan_segmenter(self):
        segmenter = DBScanSegmenter(eps=1, min_points=1)
        labels = segmenter.predict(self.pointcloud)
        self.assertEqual(len(labels), len(self.pointcloud))

    def test_base_clutter_segmenter(self):
        segmenter = BaseClutterSegmenter()
        labels = segmenter.predict(self.pointcloud)
        self.assertEqual(len(labels), len(self.pointcloud))
        self.assertTrue(np.all(labels >= 0))

    def test_plane_segmenter(self):
        plane_args = (1, 0, 0, -5)  # Plane equation x = 5
        segmenter = PlaneSegmenter(tol=0.1, alpha=0.5, plane_args=plane_args)
        labels = segmenter.predict(self.pointcloud)
        self.assertEqual(len(labels), len(self.pointcloud))
        self.assertTrue(np.all(labels >= 0))

    # @unittest.skip("Skipping Pointnet2Segmenter test due to model dependencies")
    # def test_pointnet2_segmenter(self):
    #     segmenter = Pointnet2Segmenter("path/to/model_params.pth")
    #     labels = segmenter.predict(self.pointcloud)
    #     self.assertEqual(len(labels), len(self.pointcloud))
    #     self.assertTrue(np.all(labels >= 0))


if __name__ == "__main__":
    unittest.main()
