5e90453 - fix import in the test (Murat Ambarkutuk, 2024-03-27)

7ecb832 - update the changelog (Murat Ambarkutuk, 2024-03-27)

daa714b - fix typo (Murat Ambarkutuk, 2024-03-27)

7ccfb94 - comment out pointnet2 related stuff (Murat Ambarkutuk, 2024-03-27)

85fc14d - add the report and disable the pointnet for now (Murat Ambarkutuk, 2024-03-27)

4e27438 - remove unnecessary job stages (Murat Ambarkutuk, 2024-03-27)

cf17653 - Update dockerfile to see why torch is not installed (Murat Ambarkutuk, 2024-03-27)

0dcce26 - convert test module into python package (Murat Ambarkutuk, 2024-03-27)

b0dd663 - remove the semantic class for now and add the pipeline (Murat Ambarkutuk, 2024-03-27)

e6a8457 - apply auto formatting stick with isort (Murat Ambarkutuk, 2024-03-27)

cce236d - add unit test for the segmentation module (Murat Ambarkutuk, 2024-03-27)

1109fa0 - remove unnecessary import (Murat Ambarkutuk, 2024-03-27)

8b6af03 - add docstring to the classes in the segmentation module (Murat Ambarkutuk, 2024-03-27)

9f2cc7d - add autoformatting and pre-commit checks (Murat Ambarkutuk, 2024-03-27)

b83af95 - revert changes done in the main file (Murat Ambarkutuk, 2024-03-27)

d588d08 - add code coverage with better unit testing capabilities (Murat Ambarkutuk, 2024-03-27)

ea35270 - fix typo (Murat Ambarkutuk, 2024-03-26)

74581c0 - fix typo (Murat Ambarkutuk, 2024-03-26)

da4f1d6 - Merge branch 'dev-add-semantic-classes' into 'main' (Murat Ambarkutuk, 2024-03-26)

1298e69 - add changelog and ignore the main py file with flake8 (Murat Ambarkutuk, 2024-03-26)

cd3f410 - use dind for ci pipelines (Murat Ambarkutuk, 2024-03-26)

9cc040f - reformat the code with black (Murat Ambarkutuk, 2024-03-25)

0bf92d6 - enable caching for pypi wheels (Murat Ambarkutuk, 2024-03-25)

9666d0e - fix typo (Murat Ambarkutuk, 2024-03-25)

d9e0ed4 - ci work is elaborated (Murat Ambarkutuk, 2024-03-25)

3ceb1c7 - separate torch requirements file to search in different index (Murat Ambarkutuk, 2024-03-25)

5060530 - update pipeline file (Murat Ambarkutuk, 2024-03-25)

c05f862 - use torch cpu only binaries (Murat Ambarkutuk, 2024-03-25)

2f105e9 - change unittest image (Murat Ambarkutuk, 2024-03-25)

67ba7d6 - simplify (Murat Ambarkutuk, 2024-03-24)

f86d454 - Merge branch 'main' into dev-add-semantic-classes (Murat Ambarkutuk, 2024-03-24)

9b6d197 - Merge branch '3-ci-pipeline-fails' into 'main' (Murat Ambarkutuk, 2024-03-24)

12d5238 - change .gitlab-ci to simplify (Murat Ambarkutuk, 2024-03-24)

d1a4b2f - Merge branch 'dev-update-docs' into 'main' (Murat Ambarkutuk, 2024-03-24)

7b40c9a - Add build status to the repo (Murat Ambarkutuk, 2024-03-24)

34056c0 - Merge branch 'dev-ci-cd' into 'main' (Murat Ambarkutuk, 2024-03-24)

0ff5b16 - Add .gitlab-ci.yml to enable the pipelines (Murat Ambarkutuk, 2024-03-24)

76eebe4 - update the readme adding the build information (Murat Ambarkutuk, 2024-03-24)

bdc21e2 - add contribution and licence files (Murat Ambarkutuk, 2024-03-24)

1d9aa0a - update the instructions part (Murat Ambarkutuk, 2024-03-24)

fe5a710 - add basic info (Murat Ambarkutuk, 2024-03-24)

d842fe4 - add the base classes for the semantic segmentation (Murat Ambarkutuk, 2024-03-24)

0a6b53a - Merge branch 'dev-algorithm' into 'main' (Murat Ambarkutuk, 2024-03-24)

e86fca2 - add the main algo (Murat Ambarkutuk, 2024-03-24)

2a933d0 - add build option to the makefile and change the workspace name (Murat Ambarkutuk, 2024-03-24)

ba3bd6a - Merge remote-tracking branch 'origin/main' (Murat Ambarkutuk, 2024-03-24)

bc4e6cc - Merge branch 'dependency_issue' into 'main' (Murat Ambarkutuk, 2024-03-24)

ee4b1a7 - add pointnet depedencies (Murat Ambarkutuk, 2024-03-24)

9e9500d - update .gitignore to keep the repo clean (Murat Ambarkutuk, 2024-03-20)

212820a - initial work (Murat Ambarkutuk, 2024-03-20)

40285a4 - Initial commit (Murat Ambarkutuk, 2024-03-20)
