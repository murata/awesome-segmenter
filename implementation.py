#%% imports
from enum import IntEnum

import matplotlib.pyplot as plt
import numpy as np

from segmentation.segmentation import (BaseClutterSegmenter, DBScanSegmenter,
                                       PlaneSegmenter, SegmentationPipeline)


#%% types
class Index(IntEnum):
    X = 0
    Y = 1
    Z = 2

#%% helper functions
def visualize_pointcloud_downsampled(pc:np.ndarray, downsample_factor:int=10) -> None:
    fig = plt.figure(figsize=(25, 25))
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(pc[::downsample_factor, Index.X],
               pc[::downsample_factor, Index.Y],
               pc[::downsample_factor, Index.Z],
               color="red", s=0.1)
    ax.set_xlabel("x (m)", fontsize=14)
    ax.set_ylabel("y (m)", fontsize=14)
    ax.set_zlabel("z (m)", fontsize=14)
    ax.set_xlim(-20, 20)
    ax.set_ylim(-20, 20)
    ax.set_zlim(-20, 50)
    ax.set_title("Pointcloud (3D)", fontsize=14)
    plt.show()

    # make this plot occupy 30% of the figure's width and 100% of its height
    plt.figure(figsize=(25, 25))
    plt.plot(pc[:, Index.X], pc[:, Index.Y], "rx", markersize=1, alpha=0.2)
    plt.xlabel("x (m)", fontsize=14)
    plt.ylabel("y (m)", fontsize=14)
    plt.grid(True)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title("Pointcloud (Top View)", fontsize=14)
    plt.show()

def visualize_pointcloud_downsampled_with_segment_ids(pc: np.ndarray, segment_ids: np.ndarray,
                                                      downsample_factor:int=10) -> None:
    fig = plt.figure(figsize=(25, 25))
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(pc[::downsample_factor, Index.X],
               pc[::downsample_factor, Index.Y],
               pc[::downsample_factor, Index.Z],
               c=segment_ids[::downsample_factor],
               cmap="tab20",
               s=0.2)
    ax.set_xlabel("x (m)", fontsize=14)
    ax.set_ylabel("y (m)", fontsize=14)
    ax.set_zlabel("z (m)", fontsize=14)
    ax.set_xlim(-20, 20)
    ax.set_ylim(-20, 20)
    ax.set_zlim(-20, 50)
    ax.set_title("Pointcloud (3D)", fontsize=14)
    plt.show()

    # make this plot occupy 30% of the figure's width and 100% of its height
    plt.figure(figsize=(25, 25))
    plt.scatter(pc[:, Index.X], pc[:, Index.Y], c=segment_ids, cmap="tab20", s=1, alpha=0.5)
    plt.xlabel("x (m)", fontsize=14)
    plt.ylabel("y (m)", fontsize=14)
    plt.grid(True)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title("Pointcloud (Top View)", fontsize=14)
    plt.show()

#%%
pointcloud = np.load("data/cyngn_interview_question_pointcloud_data.npy")
visualize_pointcloud_downsampled(pointcloud, downsample_factor=5) # use 'downsample_factor=1' for no downsampling during visualization

##### TODO: REQUIRES IMPLEMENTATION ##############################
##################################################################
def segment_pointcloud(pointcloud: np.ndarray) -> np.ndarray:
    # Input is a pointcloud of shape (N, 3)
    pipeline = SegmentationPipeline([
        ('clutter', BaseClutterSegmenter()),
        ('wall', PlaneSegmenter([0, 1, 0, -15])),
        ('floor', PlaneSegmenter([0, 0, 1, 1.5])),
        ('dbscan', DBScanSegmenter(1.75, 48))
    ])
    labels = pipeline(pointcloud)
    return labels

segment_ids = segment_pointcloud(pointcloud)
visualize_pointcloud_downsampled_with_segment_ids(pointcloud, segment_ids, downsample_factor=5) # use 'downsample_factor=1' for no downsampling during visualization

# %%
