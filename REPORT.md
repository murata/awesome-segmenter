# Report on Point Cloud Segmentation Approach

## Introduction

In the context of LiDAR point cloud segmentation for identifying objects in a scene, a modular and comprehensive approach has been adopted. This approach involves a pipeline of different segmenters, each targeting specific characteristics of the point cloud. This report outlines the rationale behind the choice of segmenters and details their implementation.

## Segmenters Overview

### 1. BaseSegmenter

- **Rationale:** This class serves as a foundation for all other segmenters, ensuring a consistent interface and providing common functionalities like input validation. It promotes code reusability and simplifies the integration of new segmenters.
- **Implementation:** The `BaseSegmenter` class provides placeholder methods for `fit`, `transform`, and `predict`, which are intended to be overridden by derived classes. It also includes a `validate_input` method to ensure the point cloud data is in the expected format.

### 2. DBScanSegmenter

- **Rationale:** DBSCAN is chosen for its ability to identify arbitrarily shaped clusters and handle noise in the data. It is particularly suitable for segmenting dense object clusters from sparse background points without requiring the number of clusters to be specified a priori.
- **Implementation:** The `DBScanSegmenter` class wraps the DBSCAN algorithm from scikit-learn, providing a simple interface to apply the clustering to the point cloud. The `eps` and `min_points` parameters can be adjusted to control the density threshold for cluster formation.

### 3. BaseClutterSegmenter

- **Rationale:** Clutter in point clouds, such as foliage or small debris, can interfere with object segmentation. This class aims to identify and remove such clutter based on statistical and geometrical properties.
- **Implementation:** The `BaseClutterSegmenter` class uses Open3D's `remove_statistical_outlier` and `remove_radius_outlier` methods to filter out points that are likely part of clutter. It also estimates normals and calculates incidence angles to further refine the clutter removal.

### 4. PlaneSegmenter

- **Rationale:** Identifying and segmenting planar surfaces, such as walls and floors, is crucial for structuring the point cloud data. This class is designed to detect such planes using geometric constraints.
- **Implementation:** The `PlaneSegmenter` class employs a custom method to segment planes based on the distance of points to the plane and the angle between their normals and the plane normal. It uses Open3D for normal estimation and provides adjustable parameters for tolerance and weighting.

### 5. Pointnet2Segmenter

- **Rationale:** Deep learning models like PointNet++ offer advanced capabilities for segmenting complex and diverse objects in point clouds. This class integrates such a model to leverage its learning-based segmentation power.
- **Implementation:** The `Pointnet2Segmenter` class loads a pre-trained PointNet++ model and applies it to the input point cloud. It handles the conversion of data to the appropriate format for the model and interprets the output labels.

## Segmentation Pipeline

- **Rationale:** A pipeline approach allows for the sequential application of different segmenters, each addressing specific aspects of the point cloud segmentation. This modular structure provides flexibility to add, remove, or reorder segmenters as needed.
- **Implementation:** The `SegmentationPipeline` class orchestrates the segmentation process by iterating through the list of segmenters and applying them to the point cloud. It maintains a mask to keep track of remaining points to be segmented and assigns unique segment IDs to each identified segment.

## Other Technologies Used in This Project

### Version Control and GitLab
- **Version Control:** The project employs Git for version control, allowing for efficient collaboration, code tracking, and management of changes across the development lifecycle.
- **GitLab:** GitLab serves as the central platform for repository hosting, issue tracking, and continuous integration/continuous deployment (CI/CD) pipelines. It provides an integrated environment that streamlines development workflows and enhances team collaboration.

### `Docker` and `Docker-Compose`
- **Docker:** Docker is used to containerize the application, encapsulating it with all its dependencies into a portable container. This ensures consistency across different environments, simplifies dependency management, and facilitates easy deployment and scaling.
- **Docker-Compose:** Docker-Compose is utilized for defining and running multi-container Docker applications. It allows for easy orchestration of services, enabling developers to define the application's structure and dependencies in a single file and manage them with simple commands.

### Continuous Integration
- **GitLab CI/CD:** The project leverages GitLab CI/CD for automating the build, test, and deployment processes. The CI pipeline is configured to trigger automatically upon code commits, ensuring that every change is tested and validated, leading to higher code quality and faster delivery cycles.
- **Stages:** The CI pipeline is structured into stages such as build, static-tests, unit-tests, and deploy. Each stage comprises specific jobs designed to ensure the code's integrity, security, and performance.

### Auto-Formatting and Pre-Commit Checks
- **Auto-Formatting:** Tools like `black` are integrated into the CI pipeline to automatically format the code, ensuring adherence to coding standards and improving readability.
- **Pre-Commit Checks:** Pre-commit hooks are employed to perform checks before code commits, such as syntax validation and linting. This helps catch issues early in the development process, reducing the likelihood of introducing errors into the codebase.

### Container Registry
- **GitLab Container Registry:** The project uses the GitLab Container Registry to store Docker images created during the CI process. This provides a secure and convenient way to manage and distribute container images, facilitating seamless deployment and version control.

Overall, the combination of these technologies provides a robust and efficient development environment, enabling seamless collaboration, consistent testing, and streamlined deployment processes.



## Conclusion

The chosen segmentation approach is designed to provide a comprehensive and flexible solution for point cloud segmentation in LiDAR data. Each segmenter is selected based on its ability to address specific challenges in the segmentation process, and their combination within a pipeline ensures a structured and effective segmentation outcome. The modular design also allows for easy adaptation and extension to accommodate different segmentation requirements and scenarios.
